import matplotlib.pyplot as plt

trialTypes = []
subplots = []
fig = 0
plotSetUp = False

def set_up_plot(outs):
	global fig, subplots, plotSetUp
	plotSetUp = True
	plt.ion()
	fig = plt.figure()
	plt.gca().set_xlim(0, len(outs))
	plt.gca().set_ylim(-1.0, 1.0)

def update_plot(trialType, outs):
	global fig, subplots, trialTypes
	if not plotSetUp:
		set_up_plot(outs)
	if not trialType in trialTypes:
		trialTypes.append(trialType)
		ts = range(0, len(outs), 1)
		subplot = fig.add_subplot(111).plot(ts, outs)[0]
		subplots.append(subplot)
		subplot.set_xdata(ts)
		fig.legend(subplots, trialTypes, 'upper left')
	subplot_id = trialTypes.index(trialType)
	subplots[subplot_id].set_ydata(outs)
	fig.canvas.draw()
