import numpy as np
import experiments
import plotting

#Hyper-parameters
TRIAL_GENERATOR = experiments.delayed_xor_trial #experiments.learn_invpi_trial
NEURON_COUNT = 200
MAX_WEIGHT_UPDATE = 3e-4
MAX_PERTURBATION = 0.5

TRIAL_DT = 1 #ms
TRIAL_COUNT = 10000

PERTURBATION_HZ = 3

RELAXATION_TAU = 30
CHAOS_G = 1.5
LEARNING_RATE = 0.1
REWARD_PERSIST = 0.75
EXCITE_PERSIST = 0.05
PERTURB_SCALE = 16.0

def S(x):
        return x**3

def rate(x):
        return np.tanh(x)       

def inputs():
        return trial.inputsAtTime(t) 

def network_output():
        return rate(x[-trial.OUTPUT_COUNT:]) if trial.OUTPUT_COUNT > 1 else rate(x[-1]) #subset of neurons represent network output

def reward():
        elementWiseReward = np.vectorize(trial.reward)
        return -np.average(np.abs(elementWiseReward(outs[-200:])))

def expected_reward():
        return expectedR[trial.TRIAL_TYPE] if trial.TRIAL_TYPE in expectedR else 0.0
        
def next_expected_reward():
        return (1 - REWARD_PERSIST) * reward() + REWARD_PERSIST * expected_reward()

def generate_perturbations_for_trial(trialLengthSteps):
        neurons_perturbed = np.random.binomial(1, PERTURBATION_HZ * TRIAL_DT / 1000, (trialLengthSteps, NEURON_COUNT))
        perturbations = 2 * MAX_PERTURBATION * np.random.rand(trialLengthSteps, NEURON_COUNT) - MAX_PERTURBATION
        return PERTURB_SCALE * np.multiply(perturbations, neurons_perturbed, out = pturbArr)  

def network_step_time(u, perturbations, dt):
        global t, ts, outs, x, avgx, e, J, B, r, dJavg
        avgx = (1 - EXCITE_PERSIST) * x + EXCITE_PERSIST * avgx
        perturbations[-trial.OUTPUT_COUNT:] = 0 #Don't perturb output neurons.
        perturbations[:4] = 0 #Don't perturb bias neurons.
        x[:4] = (1, 1, 1, -1) #Biases
        r = rate(x)
        x += (-x + np.dot(r, J, out = JdotrArr) + np.dot(u, B, out = BdotuArr) + perturbations) * (dt/RELAXATION_TAU)
        e += np.outer(S(r), S(x - avgx), out = xoutrArr) #Apply S inside outer product. Same as if outside, but faster.
        t += dt
        outs[t] = network_output()

def run_trial(uOft):
        global t, outs, x, e, avgx 
        t = 0
        x = np.random.rand(NEURON_COUNT) * 0.2 - 0.1 #initial noise
        avgx = np.zeros(NEURON_COUNT)
        e = np.zeros((NEURON_COUNT, NEURON_COUNT))
        perturbations = generate_perturbations_for_trial(trial.TRIAL_LENGTH)
        while t < trial.TRIAL_LENGTH:
                network_step_time(uOft[t], perturbations[t], TRIAL_DT)

def train():
        global J, t, trial, outs, x, r, avgx, e, B, expectedR, dJavg, trialInputAtT, success, trialsToSuccess
        t               = 0                                                                             #time ms
        trial           = TRIAL_GENERATOR()
        outs		= np.zeros(trial.TRIAL_LENGTH + 1)
        x               = np.zeros(NEURON_COUNT)                                                        #neuron excitations
        r               = np.zeros(NEURON_COUNT)                                                        #firing rates
        avgx            = np.zeros(NEURON_COUNT)
        e               = np.zeros((NEURON_COUNT, NEURON_COUNT))                                        #eligibilities
        J               = np.random.randn(NEURON_COUNT, NEURON_COUNT) * CHAOS_G / np.sqrt(NEURON_COUNT) #synaptic weights
        B               = np.random.rand(len(inputs()), NEURON_COUNT) * 2 - 1                           #input weights
        expectedR       = {}                                                                            #average reward for each trial typejjkkk                                                             
        dJavg           = 0.0
        successes = 0
        trialsToSuccess = 0
        for n in range(TRIAL_COUNT):
                trial = trials[n % len(trials)]
                uOft = trialInputs[n % len(trials)]
                run_trial(uOft)
                punishment = -reward()
                expectedPunishment = -expected_reward()
                expectedR[trial.TRIAL_TYPE] = next_expected_reward()

                #Weight updates
                if n > 50:
                        J -= np.clip(expectedPunishment * (punishment - expectedPunishment) * e * LEARNING_RATE, -MAX_WEIGHT_UPDATE, MAX_WEIGHT_UPDATE)
                        np.fill_diagonal(J, 0)

                #Report metadata
                successes += 1 if punishment < 1 else -successes
                if successes < 95:
                        trialsToSuccess += 1
                if n % 7 == 0:
                        plotting.update_plot(trial.TRIAL_TYPE, outs)
                if n % 23 == 0:
                        print("trial: " + str(n) + " input: " + trial.TRIAL_TYPE + " output: " + str(np.average(outs[-200:])) + " err: " + str(punishment) + " err-_err: " + str(punishment - expectedPunishment))

def main():
        timesToSuccess = []
        avgErrsAtEnd = []
        for i in range(0, 20):
                train()
                timesToSuccess.append(trialsToSuccess)
                avgErrsAtEnd.append(-np.average([rew for (trial, rew) in expectedR.items()]))
                print("Trials to success: " + str(trialsToSuccess) + " Final average error " + str(-np.average([rew for (trial, rew) in expectedR.items()])))
        print("Times to success: " + str(timesToSuccess))
        print("Average errors at end: " + str(avgErrsAtEnd))
        print("Median time to success: " + str(np.median(timesToSuccess)))
        print("Average avg error at end: " + str(np.average(avgErrsAtEnd)))
        print("25th percentile to success: " + str(np.percentile(timesToSuccess, 25.0)))
        print("75th percentile to success: " + str(np.percentile(timesToSuccess, 75.0)))

#Network state
t               = 0                                                                             #time ms
trial           = TRIAL_GENERATOR()
outs		= np.zeros(trial.TRIAL_LENGTH + 1)
x               = np.zeros(NEURON_COUNT)                                                        #neuron excitations
r               = np.zeros(NEURON_COUNT)                                                        #firing rates
avgx            = np.zeros(NEURON_COUNT)
e               = np.zeros((NEURON_COUNT, NEURON_COUNT))                                        #eligibilities
J               = np.random.randn(NEURON_COUNT, NEURON_COUNT) * CHAOS_G / np.sqrt(NEURON_COUNT) #synaptic weights
B               = np.random.rand(len(inputs()), NEURON_COUNT) * 2 - 1                           #input weights
expectedR       = {}                                                                            #average reward for each trial type                                                             
dJavg           = 0.0

successes = 0
trialsToSuccess = 0
#Cache
JdotrArr        = np.dot(r, J)
BdotuArr        = np.dot(inputs(), B)
xoutrArr        = np.outer(r ,x - avgx)
pturbArr        = np.zeros((trial.TRIAL_LENGTH, NEURON_COUNT))

trials = (TRIAL_GENERATOR(0), TRIAL_GENERATOR(1), TRIAL_GENERATOR(2), TRIAL_GENERATOR(3))
trialInputs = np.array([[curtrial.inputsAtTime(t) for t in range(0, curtrial.TRIAL_LENGTH)] for curtrial in trials])
if __name__ == "__main__":
        main()
