import numpy as np

class Trial:
	def __init__(self, inputGetter, trialType, trialLengthms, nOutputs, rewardEval):	
		self.INPUT_GETTER = inputGetter
		self.TRIAL_TYPE = trialType
		self.TRIAL_LENGTH = trialLengthms
		self.OUTPUT_COUNT = nOutputs
		self.REWARD_EVALUATOR = rewardEval

	def inputsAtTime(self, t):
		return self.INPUT_GETTER(t)

	def reward(self, outputValues):
		return self.REWARD_EVALUATOR(outputValues)


def delayed_xor_trial(seed = -1):
	seed = np.random.randint(4) if seed == -1 else seed
	input1 = seed % 2
	input2 = seed // 2
	trialType = str(input1) + str(input2)
	correct_out = 1 if input1 != input2 else -1
	return Trial(
		lambda t: (input1 if t < 200 else 0, input2 if t > 400 and t < 600 else 0),
		trialType,
		1000,
		1,
		lambda out: -abs(out - correct_out)
	)

def learn_invpi_trial():
	return Trial(
		lambda t: (0,),
		"Aim for 1/pi",
		1000,
		1,
		lambda out: -abs(out - 1.0/3.14159)
	)
